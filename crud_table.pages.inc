<?php
// $Id$

/**
*@file
*
* Menu callback for crud_table_user_page
*/
/**
* table_dolphin_user_page
*/
function crud_table_user_page($account){
	$result = db_query('SELECT * FROM {players}');
	$items = array();
	if ($result->num_rows > 0){
    // display records in a table
        $output .= '<script type="text/javascript">
    $(function() {
        $("#theList tr:even").addClass("stripe1");
        $("#theList tr:odd").addClass("stripe2");

        $("#theList tr").hover(
            function() {
                $(this).toggleClass("highlight");
            },
            function() {
                $(this).toggleClass("highlight");
            }
        );
    });
</script>
<style type="text/css">
#theList th,td {
	font-family: tahoma,Verdana, Arial, Helvetica, sans-serif;
	font-size: 16px;
	color: #000000;
	text-align:center;
}
#theList tr {
	border: 1px solid gray;
}
#theList td {
	width:160px;
	padding:3px;
}
#theList th {
	background-color:#D2E0E8;
	color:#003366
}
#theList table {
    border: 1pt solid gray;
}
.clickable {
	cursor:pointer;
}
.stripe1 {
    background-color:#0f0;
}
.stripe2 {
    background-color:#afa;
}
.highlight {
    background-color: #ffcc00;
    font-weight:bold;
}
</style><table  id="theList">';
        // set table headers
        $output .= "<thead><tr><th>ID</th><th>First Name</th><th>Last Name</th><th></th><th></th></tr> </thead><tbody>";
			while ($row = db_fetch_object($result)){
	        $output .= "<tr>";
	        $output .= "<td>" . $row->id . "</td>";
	        $output .= "<td>" . $row->firstname . "</td>";
	        $output .= "<td>" . $row->lastname . "</td>";
	        $output .= "<td><a href='crud_table/record?id=" . $row->id . "'>Edit</a></td>";
	        $output .= "<td><a href='crud_table/delete?id=" . $row->id . "'>Delete</a></td>";
	        $output .= "</tr>";
	        }
 	   $output .= "</tbody></table>";
    // if there are no records in the database, display an alert message
    }else
	 $output .= "No results to display!";
	 $output .= '<a href="crud_table/record">Add New Record</a>';
    return $output;
}
